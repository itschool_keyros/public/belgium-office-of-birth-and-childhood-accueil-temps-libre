<?php

namespace ITSchoolLib\ONEApiClientSymfony\Exceptions;

class ONEApiErrorException extends ONEApiException
{

    public function __construct(string $message, int $statusCode)
    {
        parent::__construct($message, $statusCode);
    }

}