<?php

namespace ITSchoolLib\ONEApiClientSymfony\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class ONEApiBadCredentialsException extends ONEApiException
{

    const MESSAGE = 'Invalid user credentials';

    public function __construct($message)
    {
        parent::__construct(self::MESSAGE, Response::HTTP_UNAUTHORIZED);
    }
}