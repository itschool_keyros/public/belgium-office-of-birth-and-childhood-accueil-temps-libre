<?php

namespace ITSchoolLib\ONEApiClientSymfony\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class ONEApiUndefinedCredentials extends ONEApiException
{

    const MESSAGE = 'The username and/or the password to retrieve the token are undefined';

    public function __construct()
    {
        parent::__construct(self::MESSAGE, Response::HTTP_BAD_REQUEST);
    }
}