<?php

namespace ITSchoolLib\ONEApiClientSymfony\Exceptions;


class ONEApiException extends \Exception
{

    public function __construct($message, $code)
    {
        parent::__construct($message, $code);
    }
}