# Belgium Office of Birth and Childhood - Accueil Temps Libre
Client library to easily call the ONE API
- See [the documentation](http://pro-atl-acc.one.be/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config&urls.primaryName=3.%20AES1#/2.%20D%C3%A9claration%20des%20pr%C3%A9sences/createAes1Presences).
- The library allows you:
  - to retrieves <b>the places</b> you have access to
  - to retrieves the number of <b>presences</b> per day
  - to encode the number of <b>presences</b> per day
  - to retrieve the number of <b>inscriptions</b> per trimester
  - to encode the number of <b>inscriptions</b> per trimester
  - to ask for a <b>subsidy</b>
  - to retrieve the <b>subsidy</b> demand status


## Requirements
- Symfony 4.4 or above
- An account to have acces to the [ONE Platform](https://login-acc.one.be/auth/realms/ONE/protocol/openid-connect/auth?client_id=pro-atl&redirect_uri=https%3A%2F%2Fpro-atl-acc.one.be%2F&state=fdcde4ba-5f0e-4ee3-87a2-95c1294f0a05&response_mode=fragment&response_type=code&scope=openid&nonce=3c70135a-2310-4ed3-8bf0-7b501b5267f2&code_challenge=EsvXig-tNTQcq174UBYZaAFjp_q-upfzRAaQ5iZqVh0&code_challenge_method=S256)
- Create a place and schedules on the ONE Platform.

## Installation

- Add in your composer.json repositories :
  ``` json
  { 
      "type": "vcs",
      "url": "https://gitlab.com/itschool_keyros/public/belgium-office-of-birth-and-childhood-accueil-temps-libre"
  } 
  ```

  ```bash 
    composer require itschool-lib/one-api-client-symfony "dev-main"
  ```


## Usage
- Call the library with autowiring
``` php
  private ONEApiService $ONEApiService;

  public function __construct(ONEApiService $ONEApiService) 
  {
      $this->ONEApiService = $ONEApiService;
  }
  ```

- Before the first request you must init the credentials by calling initAuth  
```php 
  $this->ONEApiService->initAuth('userName', 'password');
```


- Now you can make a request <br>
```php
  $placesDTOList = $this->ONEApiService->getPlaces();
```


## To run the tests
``` bash 
php7.4 vendor/phpunit/phpunit/phpunit --configuration phpunit.xml.dist
```

## Authors
- Nicolas Lambert
- Simon Mohimont
