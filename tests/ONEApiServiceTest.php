<?php

namespace ITSchoolLib\ONEApiClientSymfony\Tests;

use ITSchoolLib\ONEApiClientSymfony\DTO\NumberPresencesDTO;
use ITSchoolLib\ONEApiClientSymfony\DTO\PlaceDTO;
use ITSchoolLib\ONEApiClientSymfony\Exceptions\ONEApiBadCredentialsException;
use ITSchoolLib\ONEApiClientSymfony\Exceptions\ONEApiErrorException;
use ITSchoolLib\ONEApiClientSymfony\Exceptions\ONEApiUndefinedCredentials;
use ITSchoolLib\ONEApiClientSymfony\Lib\BearerTokenCacheService;
use ITSchoolLib\ONEApiClientSymfony\Lib\ONEApiService;
use ITSchoolLib\ONEApiClientSymfony\Tests\Lib\FakeHttpClient;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


/**
 * @see https://smaine-milianni.medium.com/mock-the-symfony-httpclient-a1b2451430ef
 */
class ONEApiServiceTest extends WebTestCase
{

    private ONEApiService $ONEApiService;
    private BearerTokenCacheService $bearerTokenCacheService;
    private FakeHttpClient $fakeHttpClient;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->fakeHttpClient = self::$container->get(FakeHttpClient::class);
        $this->ONEApiService = self::$container->get(ONEApiService::class);
        $this->bearerTokenCacheService = self::$container->get(BearerTokenCacheService::class);
    }

    /**
     * test function getPlaces
     * return a PlaceDtoList
     *
     * expect to have 1 placeDTO
     * expect to have the right placeRef
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testGetPlaces()
    {
        $placesResponse = [
            "_embedded" => [
                "aes1PlaceList" => [
                    [
                        "externalId" => "EXTERNAL_REF",
                        "name" => "School Example",
                        "address" => [
                            "street" => "Example street",
                            "streetNumber" => "99",
                            "postalCode" => "9999",
                            "municipality" => "Test",
                            "locality" => "Test",
                            "country" => "BE",
                        ],
                    ],
                ],
            ],
            "page" => [
                "size" => 50,
                "totalElements" => 1,
                "totalPages" => 1,
                "number" => 0,
            ],
        ];

        // create each response that matches an URL 
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            ONEApiService::URL_GET_PLACES => new MockResponse(json_encode($placesResponse)),
        ];

        $this->fakeHttpClient->setResponses($responses);

        // clear the bearer token cache
        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('username', 'password');
        $placesDTO = $this->ONEApiService->getPlaces();

        self::assertCount(1, $placesDTO);
        /** @var PlaceDTO $placeDTO */
        $placeDTO = $placesDTO[0];
        self::assertEquals("EXTERNAL_REF", $placeDTO->place_ref);
    }


    /**
     * test function getNumberOfInscriptions
     * return a NumberOfInscriptionsDTO
     *
     * expect to have the right number of inscriptions
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testGetNumberOfInscriptions()
    {
        $numberInscriptionsResponse = [
            "nbEnfantsInscritsMaternelle" => 36,
            "nbEnfantsInscritsPrimaire" => 108,
            "comment" => "test",
        ];

        $placeRef = 'placeRef';
        $year = 2022;
        $trimester = 1;

        // create each response that matches an url
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            // url to get number of inscriptions
            ONEApiService::ONE_API_ENDPOINT . "api/aes1/$placeRef/inscriptions/year/$year/trimester/$trimester" => new MockResponse(
                json_encode($numberInscriptionsResponse)
            ),
        ];

        $this->fakeHttpClient->setResponses($responses);

        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('username', 'password');
        $numberOfInscriptionsDTO = $this->ONEApiService->getNumberOfInscriptions($placeRef, $year, $trimester);

        self::assertEquals(36, $numberOfInscriptionsDTO->number_children_kindergarten);
        self::assertEquals(108, $numberOfInscriptionsDTO->number_children_primary);
        self::assertEquals('test', $numberOfInscriptionsDTO->comment);
    }

    /**
     * test updateNumberInscriptions
     * expect status code = 200
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testUpdateNumberInscriptions()
    {
        $placeRef = 'placeRef';
        $year = 2022;
        $trimester = 1;

        $numberOfInscriptions = [
            'nbEnfantsInscritsMaternelle' => 5,
            'nbEnfantsInscritsPrimaire' => 8,
            'comment' => 'test',
        ];

        // create each response that matches an url
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            // url to put number of inscriptions
            ONEApiService::ONE_API_ENDPOINT . "api/aes1/$placeRef/inscriptions/year/$year/trimester/$trimester" => new MockResponse(''),
        ];

        $this->fakeHttpClient->setResponses($responses);

        $this->ONEApiService->initAuth('username', 'password');
        $response = $this->ONEApiService->updateNumberInscriptions($placeRef, $year, $trimester, $numberOfInscriptions);

        self::assertEquals('', $response->getContent());
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * test getNumberOfPresences
     * return NumberPresencesDTOList
     *
     * expect to have 2 NumberPresencesDTO
     * expect to have the right number of presences
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testGetNumberOfPresences()
    {
        $numberPresencesResponse = [
            [
                "presenceEnfant" => 7,
                "presenceEnfantDP" => 0,
                "datePresence" => "2022-02-01",
            ],
            [
                "presenceEnfant" => 9,
                "presenceEnfantDP" => 0,
                "datePresence" => "2022-02-02",
            ],
        ];

        $placeRef = 'placeRef';
        $year = 2022;
        $month = 1;

        // create each response that matches an url
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            // url to get number of presences
            ONEApiService::ONE_API_ENDPOINT . "api/aes1/$placeRef/presences/year/$year/month/$month" => new MockResponse(json_encode($numberPresencesResponse)),
        ];

        $this->fakeHttpClient->setResponses($responses);

        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('wrongUsername', 'wrongPassword');
        $numberOfPresencesDTOList = $this->ONEApiService->getNumberOfPresences($placeRef, $year, $month);
        self::assertCount(2, $numberOfPresencesDTOList);

        /** @var NumberPresencesDTO $numberOfPresencesDTO */
        $numberOfPresencesDTO = $numberOfPresencesDTOList[0];

        self::assertEquals(7, $numberOfPresencesDTO->presences_children);
        self::assertEquals(0, $numberOfPresencesDTO->presences_children_dp);
        self::assertEquals('2022-02-01', $numberOfPresencesDTO->date);
    }

    /**
     * test UpdateNumberPresences
     * expect to have status code = 200
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testUpdateNumberPresences()
    {
        $placeRef = 'placeRef';

        $numberOfPresences = [
            [
                'datePresence' => '2022-01-01',
                'presenceEnfant' => 5,
            ],
            [
                'datePresence' => '2022-01-02',
                'presenceEnfant' => 4,
            ],
        ];

        // create each response that matches an url
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            // url to put number of inscriptions
            ONEApiService::ONE_API_ENDPOINT . "api/aes1/$placeRef/presences" => new MockResponse(''),
        ];

        $this->fakeHttpClient->setResponses($responses);

        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('username', 'password');
        $response = $this->ONEApiService->updateNumberOfPresences($placeRef, $numberOfPresences);

        self::assertEquals('', $response->getContent());
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * test getSubsidyDemandStatus
     * that retrieves the status of the subsidy demand
     *
     * expect to have the right status
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testGetSubsidyDemandStatus()
    {
        $placeRef = 'placeRef';
        $year = 2022;
        $trimester = 1;

        $subsidyStatusResponse = [
            "anneeCalendrier" => 2021,
            "trimestre" => 4,
            "status" => "NON_DEMANDE"
        ];

        // create each response that matches an url
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            // url to get subsidy status
            ONEApiService::ONE_API_ENDPOINT . "api/aes1/$placeRef/subventions/year/$year/trimester/$trimester" => new MockResponse(json_encode($subsidyStatusResponse)),
        ];

        $this->fakeHttpClient->setResponses($responses);

        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('username', 'password');
        $subsidyDTO = $this->ONEApiService->getSubsidyDemandStatus($placeRef, $year, $trimester);

        self::assertEquals(2021, $subsidyDTO->year);
        self::assertEquals(4, $subsidyDTO->trimester);
        self::assertEquals("NON_DEMANDE", $subsidyDTO->status);
    }

    /**
     * test CreateSubsidyDemand
     * that creates a subsidy demand
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testCreateSubsidyDemand()
    {
        $placeRef = 'placeRef';
        $year = 2022;
        $trimester = 1;

        // create each response that matches an url
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            // url to create a subsidy demand
            ONEApiService::ONE_API_ENDPOINT . "api/aes1/$placeRef/subventions" => new MockResponse('', ['http_code' => Response::HTTP_CREATED]),
        ];

        $this->fakeHttpClient->setResponses($responses);

        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('username', 'password');

        $response = $this->ONEApiService->createSubsidyDemand($placeRef, $year, $trimester);
        self::assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        self::assertEquals('', $response->getContent());
    }

    /**
     * test function getPlaces
     * with wrong credentials
     * expect to have an exception with an error message
     * @return void
     * @throws DecodingExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws TransportExceptionInterface|ONEApiErrorException|ONEApiUndefinedCredentials
     */
    public function testInvalidCredentialsForToken()
    {
        $getBearerTokenReponse = [
            "error" => "invalid_grant",
            "error_description" => "Invalid user credentials",
        ];

        // create each response that matches an URL
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode($getBearerTokenReponse), ['http_code' => Response::HTTP_UNAUTHORIZED]),
        ];

        // set the response we want to the FakeHttpClient
        $this->fakeHttpClient->setResponses($responses);


        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('wrongUsername', 'wrongPassword');

        $this->expectException(ONEApiBadCredentialsException::class);
        $this->expectExceptionMessage('Invalid user credentials');
        $this->ONEApiService->getPlaces();
    }

    /**
     * test getPlaces
     * the request to get the bearer token returned an internal server error
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testRequestWithBearerTokenReturnError()
    {
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['Internal Server Error']), ['http_code' => Response::HTTP_INTERNAL_SERVER_ERROR])
        ];

        // set the response we want to the FakeHttpClient
        $this->fakeHttpClient->setResponses($responses);

        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('username', 'password');

        $this->expectException(ONEApiErrorException::class);
        $this->expectExceptionCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        $this->expectExceptionMessage(json_encode(['Internal Server Error']));
        $this->ONEApiService->getPlaces();
    }

    /**
     * test getPlaces
     * without calling initAuth
     * expect Exception
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiErrorException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|ONEApiBadCredentialsException
     */
    public function testUndefinedCredentials()
    {
        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->expectException(ONEApiUndefinedCredentials::class);
        $this->expectExceptionMessage(ONEApiUndefinedCredentials::MESSAGE);
        $this->ONEApiService->getPlaces();
    }

    /**
     * test post subsidy demand
     * with a non-existing placeRef
     *
     * expect exception
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testRequestReturnError()
    {
        $placeRef = 'wrongPlaceRef';
        $year = 2021;
        $trimester = 1;

        $responseError = [
            "ERROR : Le lieu AES1 ayant référence externe wrongPlaceRef n'existe pas"
        ];

        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            ONEApiService::ONE_API_ENDPOINT . "api/aes1/$placeRef/subventions" =>
                new MockResponse(json_encode($responseError), ['http_code' => Response::HTTP_BAD_REQUEST]),
        ];

        // set the response we want to the FakeHttpClient
        $this->fakeHttpClient->setResponses($responses);


        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('username', 'password');

        $this->expectException(ONEApiErrorException::class);
        $this->expectExceptionCode(Response::HTTP_BAD_REQUEST);
        $this->expectExceptionMessage(json_encode($responseError));
        $this->ONEApiService->createSubsidyDemand($placeRef, $year, $trimester);
    }

    /**
     * test getPlaces
     * request return 401
     * expect right Exception
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testRequestReturnUnauthorized()
    {
        $responses = [
            ONEApiService::URL_BEARER_TOKEN => new MockResponse(json_encode(['access_token' => 'token'])),
            ONEApiService::URL_GET_PLACES => new MockResponse('', ['http_code' => Response::HTTP_UNAUTHORIZED]),
        ];

        // set the response we want to the FakeHttpClient
        $this->fakeHttpClient->setResponses($responses);

        $this->bearerTokenCacheService->clearCachedBearerToken();

        $this->ONEApiService->initAuth('username', 'password');

        $this->expectException(ONEApiErrorException::class);
        $this->expectExceptionCode(Response::HTTP_UNAUTHORIZED);
        $this->expectExceptionMessage('');
        $this->ONEApiService->getPlaces();
    }
}