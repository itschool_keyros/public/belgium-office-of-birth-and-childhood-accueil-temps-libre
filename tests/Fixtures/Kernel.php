<?php

namespace ITSchoolLib\ONEApiClientSymfony\Tests\Fixtures;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    private const CONFIG_EXTS = '.{php,xml,yaml,yml}';

    public function registerBundles(): iterable
    {
        $bundles = [
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
        ];

        return $bundles;
    }

    public function getProjectDir(): string
    {
        return \dirname(__DIR__);
    }


    /**
     * @param \Symfony\Component\Config\Loader\LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $_ENV['APP_ENV'] = 'test';
        $loader->load($this->getProjectDir().'/../Resources/config/{services}'.self::CONFIG_EXTS, 'glob');
        $loader->load($this->getProjectDir().'/Fixtures/config/config_'.$this->environment.self::CONFIG_EXTS, 'glob');
        $loader->load($this->getProjectDir().'/Fixtures/config/{services}_'.$this->environment.self::CONFIG_EXTS, 'glob');
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader): void
    {
////        $container->addResource(new FileResource($this->getProjectDir().'/config/bundles.php'));
//        $container->setParameter('container.dumper.inline_class_loader', \PHP_VERSION_ID < 70400 || $this->debug);
//        $container->setParameter('container.dumper.inline_factories', true);
//        $confDir = $this->getProjectDir().'/Fixtures/config';
//
//        $loader->load($confDir.'/{packages}/*'.self::CONFIG_EXTS, 'glob');
//        $loader->load($confDir.'/{packages}/'.$this->environment.'/*'.self::CONFIG_EXTS, 'glob');
//        $loader->load($confDir.'/{services}'.self::CONFIG_EXTS, 'glob');
//        $loader->load($confDir.'/{services}_'.$this->environment.self::CONFIG_EXTS, 'glob');
    }



    /**
     * @return string
     */
    public function getCacheDir()
    {
        return realpath(__DIR__ . '/../../') . '/var/' . $this->environment . '/cache';
    }

    public function getLogDir()
    {
        return realpath(__DIR__ . '/../../') . '/var/' . $this->environment . '/logs';
    }

    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        // TODO: Implement configureRoutes() method.
    }
}
