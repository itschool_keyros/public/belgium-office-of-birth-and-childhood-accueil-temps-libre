<?php

namespace ITSchoolLib\ONEApiClientSymfony\Tests;

use ITSchoolLib\ONEApiClientSymfony\Lib\BearerTokenCacheService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BearerTokenCacheServiceTest extends WebTestCase
{

    private BearerTokenCacheService $bearerTokenCacheService;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->bearerTokenCacheService = self::$container->get(BearerTokenCacheService::class);
    }

    /**
     * test addBearerTokenToCache
     * that add the bearer token to the cache
     * @return void
     * @throws InvalidArgumentException
     */
    public function testAddBearerTokenToCache()
    {
        // start by clearing the cache
        $this->bearerTokenCacheService->clearCachedBearerToken();

        // get cache
        $cache = $this->bearerTokenCacheService->getCachedBearerToken();

        // expect cache item to be null
        self::assertNull($cache->get());

        // expect item lookup don't resulted in a cache hit
        self::assertFalse($cache->isHit());

        // add item to cache
        $token = 'bearerToken';
        $cache->set($token);
        $this->bearerTokenCacheService->addBearerTokenToCache($cache);

        // get cache
        $cache = $this->bearerTokenCacheService->getCachedBearerToken();

        // expect token to be in the cache
        self::assertEquals($token, $cache->get());
    }

    /**
     * test clearCachedBearerToken
     * that clear the content of the cache for the bearerToken
     * @return void
     * @throws InvalidArgumentException
     */
    public function testClearCachedBearerToken()
    {
        // get cache
        $cache = $this->bearerTokenCacheService->getCachedBearerToken();

        // add item to cache
        $token = 'bearerToken';
        $cache->set($token);
        $this->bearerTokenCacheService->addBearerTokenToCache($cache);

        // get cache
        $cache = $this->bearerTokenCacheService->getCachedBearerToken();

        // expect token to be in the cache
        self::assertEquals($token, $cache->get());

        //clear the cache
        $cache = $this->bearerTokenCacheService->clearCachedBearerToken();

        // get cache
        $cache = $this->bearerTokenCacheService->getCachedBearerToken();

        // expect cache item to be null
        self::assertNull($cache->get());

        // expect item lookup don't resulted in a cache hit
        self::assertFalse($cache->isHit());
    }
}