<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\Tests\Lib;

use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\ResponseStreamInterface;

// this client will be used in our test
final class FakeHttpClient implements HttpClientInterface
{
    /**
     *
     * We create an array of response with the $url as "key" and $response as "value"
     * ex: $responses = [
     *      '/external/user_exists' => new MockResponse($content)
     * ];
     */
    private array $responses;

    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        // Get the response by accessing to the "key"
        $response = $this->responses[$url] ?? null;

        if (null === $response) {
            throw new \LogicException(sprintf('There is no response for url: %s', $url));
        }

        return (new MockHttpClient($response, 'https://user_service_api.fake'))->request($method, $url);

    }

    public function stream($responses, float $timeout = null): ResponseStreamInterface
    {
        throw new \LogicException(sprintf('%s() is not implemented', __METHOD__));
    }


    public function withOptions(): self
    {
        return $this;
    }

    public function setResponses(array $responses)
    {
        $this->responses = $responses;
    }
}