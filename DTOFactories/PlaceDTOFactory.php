<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\DTOFactories;

use ITSchoolLib\ONEApiClientSymfony\DTO\PlaceDTO;

class PlaceDTOFactory
{
    public function buildDTO(array $place): PlaceDTO
    {
        $dto = new PlaceDTO();
        $dto->place_ref = $place['externalId'];
        $dto->name = $place['name'];

        $address = $place['address'];
        $dto->street = $address['street'];
        $dto->street_number = (int)$address['streetNumber'];
        $dto->postal_code = (int)$address['postalCode'];
        $dto->locality = $address['locality'];
        $dto->municipality = $address['municipality'];
        $dto->country = $address['country'];

        return $dto;
    }
}