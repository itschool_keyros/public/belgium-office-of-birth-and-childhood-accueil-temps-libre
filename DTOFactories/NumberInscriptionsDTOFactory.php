<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\DTOFactories;

use ITSchoolLib\ONEApiClientSymfony\DTO\NumberInscriptionsDTO;

class NumberInscriptionsDTOFactory
{
    public function buildDTO(array $numberInscriptions, string $placeRef, int $year, int $trimester): NumberInscriptionsDTO
    {
        $dto = new NumberInscriptionsDTO();
        $dto->place_ref = $placeRef;
        $dto->year = $year;
        $dto->trimester = $trimester;
        $dto->number_children_kindergarten = $numberInscriptions['nbEnfantsInscritsMaternelle'];
        $dto->number_children_primary = $numberInscriptions['nbEnfantsInscritsPrimaire'];
        $dto->comment = $numberInscriptions['comment'];

        return $dto;
    }
}