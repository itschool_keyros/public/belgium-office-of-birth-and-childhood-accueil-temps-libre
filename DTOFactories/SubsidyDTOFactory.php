<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\DTOFactories;

use ITSchoolLib\ONEApiClientSymfony\DTO\SubsidyDTO;

class SubsidyDTOFactory
{
    public function buildDTO(array $subsidy, string $placeRef): SubsidyDTO
    {
        $dto = new SubsidyDTO();
        $dto->year = $subsidy['anneeCalendrier'];
        $dto->trimester = $subsidy['trimestre'];
        $dto->status = $subsidy['status'];
        $dto->place_ref = $placeRef;

        return $dto;
    }
}