<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\DTOFactories;

use ITSchoolLib\ONEApiClientSymfony\DTO\NumberPresencesDTO;

class NumberPresencesDTOFactory
{
    public function buildDTO(array $presences, string $placeRef, int $year, int $month): NumberPresencesDTO
    {
        $dto = new NumberPresencesDTO();
        $dto->place_ref = $placeRef;
        $dto->year = $year;
        $dto->month = $month;
        $dto->presences_children = $presences['presenceEnfant'];
        $dto->presences_children_dp = $presences['presenceEnfantDP'];
        $dto->date = $presences['datePresence'];

        return $dto;
    }
}