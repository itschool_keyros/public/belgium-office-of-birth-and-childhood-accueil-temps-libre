<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\DTO;

class PlaceDTO
{
    /**
     * @Serializer\Type("string")
     */
    public string $place_ref;

    /**
     * @Serializer\Type("string")
     */
    public string $name;

    /**
     * @Serializer\Type("string")
     */
    public string $street;

    /**
     * @Serializer\Type("int")
     */
    public int $street_number;

    /**
     * @Serializer\Type("int")
     */
    public int $postal_code;

    /**
     * @Serializer\Type("string")
     */
    public string $municipality;

    /**
     * @Serializer\Type("string")
     */
    public string $locality;

    /**
     * @Serializer\Type("string")
     */
    public string $country;
}