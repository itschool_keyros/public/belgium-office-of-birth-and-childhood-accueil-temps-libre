<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\DTO;

class SubsidyDTO
{
    /**
     * @Serializer\Type("string")
     */
    public string $place_ref;

    /**
     * @Serializer\Type("int")
     */
    public int $year;

    /**
     * @Serializer\Type("int")
     */
    public int $trimester;

    /**
     * @Serializer\Type("string")
     */
    public string $status;
}