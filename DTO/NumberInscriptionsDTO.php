<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\DTO;

class NumberInscriptionsDTO
{
    /**
     * @Serializer\Type("int")
     */
    public int $number_children_kindergarten;

    /**
     * @Serializer\Type("int")
     */
    public int $number_children_primary;

    /**
     * @Serializer\Type("string")
     */
    public ?string $comment = null;

    /**
     * @Serializer\Type("string")
     */
    public string $place_ref;

    /**
     * @Serializer\Type("int")
     */
    public int $year;

    /**
     * @Serializer\Type("int")
     */
    public int $trimester;
}