<?php
declare(strict_types=1);

namespace ITSchoolLib\ONEApiClientSymfony\DTO;

class NumberPresencesDTO
{
    /**
     * @Serializer\Type("string")
     */
    public string $date;

    /**
     * @Serializer\Type("int")
     */
    public int $presences_children;

    /**
     * @Serializer\Type("int")
     */
    public int $presences_children_dp;

    /**
     * @Serializer\Type("string")
     */
    public string $place_ref;

    /**
     * @Serializer\Type("int")
     */
    public int $year;

    /**
     * @Serializer\Type("int")
     */
    public int $month;
}