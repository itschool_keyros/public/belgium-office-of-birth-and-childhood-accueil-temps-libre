<?php

namespace ITSchoolLib\ONEApiClientSymfony\Lib;


use ITSchoolLib\ONEApiClientSymfony\DTO\NumberInscriptionsDTO;
use ITSchoolLib\ONEApiClientSymfony\DTO\SubsidyDTO;
use ITSchoolLib\ONEApiClientSymfony\DTOFactories\NumberInscriptionsDTOFactory;
use ITSchoolLib\ONEApiClientSymfony\DTOFactories\NumberPresencesDTOFactory;
use ITSchoolLib\ONEApiClientSymfony\DTOFactories\PlaceDTOFactory;
use ITSchoolLib\ONEApiClientSymfony\DTOFactories\SubsidyDTOFactory;
use ITSchoolLib\ONEApiClientSymfony\Exceptions\ONEApiBadCredentialsException;
use ITSchoolLib\ONEApiClientSymfony\Exceptions\ONEApiUndefinedCredentials;
use ITSchoolLib\ONEApiClientSymfony\Exceptions\ONEApiErrorException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class ONEApiService
{
    public const ONE_API_ENDPOINT = 'https://pro-atl.one.be/';
    public const URL_GET_PLACES = self::ONE_API_ENDPOINT . 'api/aes1/places?page=0&size=0';
    public const URL_BEARER_TOKEN = 'https://login.one.be/auth/realms/ONE/protocol/openid-connect/token';

    private const METHOD_GET = 'GET';
    private const METHOD_POST = 'POST';
    private const METHOD_PUT = 'PUT';

    private HttpClientInterface $httpClient;
    private PlaceDTOFactory $placeDTOFactory;
    private NumberInscriptionsDTOFactory $numberInscriptionsDTOFactory;
    private NumberPresencesDTOFactory $numberPresencesDTOFactory;
    private SubsidyDTOFactory $subsidyDTOFactory;
    private BearerTokenCacheService $bearerTokenCacheService;

    private string $apiUsername = '';
    private string $apiPassword = '';

    public function __construct(
        HttpClientInterface $httpClient,
        PlaceDTOFactory $placeDTOFactory,
        NumberInscriptionsDTOFactory $numberInscriptionsDTOFactory,
        NumberPresencesDTOFactory $numberPresencesDTOFactory,
        SubsidyDTOFactory $subsidyDTOFactory,
        BearerTokenCacheService $bearerTokenCacheService
    )
    {
        $this->httpClient = $httpClient;
        $this->placeDTOFactory = $placeDTOFactory;
        $this->numberInscriptionsDTOFactory = $numberInscriptionsDTOFactory;
        $this->numberPresencesDTOFactory = $numberPresencesDTOFactory;
        $this->subsidyDTOFactory = $subsidyDTOFactory;
        $this->bearerTokenCacheService = $bearerTokenCacheService;
    }

    /**
     * Must be called before a request
     *
     * init class property with the username and password
     * used to retrieve the bearer token
     * @param string $username
     * @param string $password
     * @return void
     */
    public function initAuth(string $username, string $password)
    {
        $this->apiUsername = $username;
        $this->apiPassword = $password;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $body
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function makeRequest(string $method, string $url, array $body = []): ResponseInterface
    {
        $options = [
            'headers' => $this->buildHeader($method),
        ];

        if (count($body) > 0) {
            $options['body'] = json_encode($body);
        }

        $response = $this->httpClient->request($method, $url, $options);

        if ($response->getStatusCode() === Response::HTTP_UNAUTHORIZED) {
            // if statusCode = 401 => clear the cache, get a new bearerToken, try again
            $this->bearerTokenCacheService->clearCachedBearerToken();
            $options["headers"] = $this->buildHeader($method);

            $response = $this->httpClient->request($method, $url, $options);
        }

        $successCode = [Response::HTTP_OK, Response::HTTP_CREATED];

        if (in_array($response->getStatusCode(), $successCode) === false ) {
            throw new ONEApiErrorException($response->getContent(false), $response->getStatusCode());
        }

        return $response;
    }


    /**
     * Return the bearer token from the cache
     * or make a request to retrieve a new token
     * @return string
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getBearerToken(): string
    {
        $bearerTokenCache = $this->bearerTokenCacheService->getCachedBearerToken();
        if ($bearerTokenCache->isHit() === false) {
            // if the token is not stored in the cache

            if ($this->apiUsername === '' || $this->apiPassword === '') {
                throw new ONEApiUndefinedCredentials();
            }

            $header = [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ];

            $body = [
                'username' => $this->apiUsername,
                'password' => $this->apiPassword,
                'grant_type' => 'password',
                'client_id' => 'pro-atl',
            ];

            $response = $this->httpClient->request(
                'POST',
                self::URL_BEARER_TOKEN,
                [
                    'headers' => $header,
                    'body' => $body,
                ]
            );

            if ($response->getStatusCode() !== Response::HTTP_OK) {
                if ($response->getStatusCode() === Response::HTTP_UNAUTHORIZED) {
                    throw new ONEApiBadCredentialsException($response->getContent(false));
                }
                throw new ONEApiErrorException($response->getContent(false), $response->getStatusCode());
            }

            $bearerToken = $response->toArray()['access_token'];

            $bearerTokenCache->set($bearerToken);
            $this->bearerTokenCacheService->addBearerTokenToCache($bearerTokenCache);
        } else {
            // if the item is in the cache, get it
            $bearerToken = $bearerTokenCache->get();
        }

        return $bearerToken;
    }

    /**
     * build common header for all request (except authentification)
     * @param string $method
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function buildHeader(string $method): array
    {
        $bearer = $this->getBearerToken();
        $header = [
            'Authorization' => "Bearer $bearer",
        ];
        if ($method === self::METHOD_POST || $method === self::METHOD_PUT) {
            // for post and put requests
            $header['Content-type'] = 'application/json';
        }

        return $header;
    }

    /**
     * Method that returns an array of AES12 places DTO
     * @see https://pro-atl-acc.one.be/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config&urls.primaryName=3.%20AES1#/
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getPlaces(): array
    {
        $response = $this->makeRequest(self::METHOD_GET, self::URL_GET_PLACES);

        return $this->convertResponseInPlacesList($response->toArray());
    }

    /**
     * convert the response in an array of PlaceDTO
     * @param array $responseContent
     * @return array
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     */
    public function convertResponseInPlacesList(array $responseContent): array
    {
        $places = $responseContent['_embedded']['aes1PlaceList'];
        $placesList = [];
        foreach ($places as $place) {
            $placesList[] = $this->placeDTOFactory->buildDTO($place);
        }

        return $placesList;
    }


    /**
     * Method that returns an NumberInscriptions DTO
     * @see https://pro-atl-acc.one.be/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config&urls.primaryName=3.%20AES1#/
     * @param string $placeRef
     * @param int $year
     * @param int $trimester
     * @return NumberInscriptionsDTO
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getNumberOfInscriptions(string $placeRef, int $year, int $trimester): NumberInscriptionsDTO
    {
        $url = self::ONE_API_ENDPOINT . "api/aes1/$placeRef/inscriptions/year/$year/trimester/$trimester";
        $response = $this->makeRequest(self::METHOD_GET, $url);

        return $this->numberInscriptionsDTOFactory->buildDTO($response->toArray(), $placeRef, $year, $trimester);
    }


    /**
     * Method that updates the number of inscriptions and return the response of the request
     * @see https://pro-atl-acc.one.be/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config&urls.primaryName=3.%20AES1#/
     * @param string $placeRef
     * @param int $year
     * @param int $trimester
     * @param array $numberInscriptions
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function updateNumberInscriptions(string $placeRef, int $year, int $trimester, array $numberInscriptions): ResponseInterface
    {
        $url = self::ONE_API_ENDPOINT . "api/aes1/$placeRef/inscriptions/year/$year/trimester/$trimester";

        return $this->makeRequest(self::METHOD_PUT, $url, $numberInscriptions);
    }


    /**
     * Method that returns a list of NumberPresencesDTO
     * @see https://pro-atl-acc.one.be/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config&urls.primaryName=3.%20AES1#/
     * @param string $placeRef
     * @param int $year
     * @param int $month
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getNumberOfPresences(string $placeRef, int $year, int $month): array
    {
        $url = self::ONE_API_ENDPOINT . "api/aes1/$placeRef/presences/year/$year/month/$month";

        $response = $this->makeRequest(self::METHOD_GET, $url);

        return $this->convertResponseInNumberPresencesDTOList($response->toArray(), $placeRef, $year, $month);
    }

    /**
     * convert the response from the request to an array of NumberPresencesDTO
     * @param array $numberPresencesList
     * @param string $placeRef
     * @param int $trimester
     * @param int $month
     * @return array
     */
    public function convertResponseInNumberPresencesDTOList(array $numberPresencesList, string $placeRef, int $trimester, int $month): array
    {
        $numberPresencesDTOList = [];
        foreach ($numberPresencesList as $numberPresences) {
            $numberPresencesDTOList[] = $this->numberPresencesDTOFactory->buildDTO($numberPresences, $placeRef, $trimester, $month);
        }

        return $numberPresencesDTOList;
    }


    /**
     * Method that updates the number of presences and return response from the request
     * @see https://pro-atl-acc.one.be/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config&urls.primaryName=3.%20AES1#/
     * @param string $placeRef
     * @param array $presences
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function updateNumberOfPresences(string $placeRef, array $presences): ResponseInterface
    {
        $url = self::ONE_API_ENDPOINT . "api/aes1/$placeRef/presences";

        return $this->makeRequest(self::METHOD_PUT, $url, $presences);
    }

    /**
     * Method that returns a Subsidy DTO
     * @see https://pro-atl-acc.one.be/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config&urls.primaryName=3.%20AES1#/
     * @param string $placeRef
     * @param int $year
     * @param int $trimester
     * @return SubsidyDTO
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getSubsidyDemandStatus(string $placeRef, int $year, int $trimester): SubsidyDTO
    {
        $url = self::ONE_API_ENDPOINT . "api/aes1/$placeRef/subventions/year/$year/trimester/$trimester";

        $response = $this->makeRequest(self::METHOD_GET, $url);
        return $this->subsidyDTOFactory->buildDTO($response->toArray(), $placeRef);
    }

    /** method that creates a subsidy demand
     * @param string $placeRef
     * @param int $year
     * @param int $trimester
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws ONEApiBadCredentialsException
     * @throws ONEApiErrorException
     * @throws ONEApiUndefinedCredentials
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function createSubsidyDemand(string $placeRef, int $year, int $trimester): ResponseInterface
    {
        $url = self::ONE_API_ENDPOINT . "api/aes1/$placeRef/subventions";

        $body = [
            'anneeCalendrier' => $year,
            'trimestre' => $trimester
        ];

        return $this->makeRequest(self::METHOD_POST, $url, $body);
    }
}