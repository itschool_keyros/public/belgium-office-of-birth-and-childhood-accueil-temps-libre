<?php

namespace ITSchoolLib\ONEApiClientSymfony\Lib;

use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\Cache\ItemInterface;

class BearerTokenCacheService
{
    private const BUSINESS_CACHE = '/var/cache/business_cache';
    private const LIFE_TIME = 4 * 3600; // 4 hours
    private const NAMESPACE = 'ONEApi';
    private const KEY_NAME = 'bearerToken';

    private KernelInterface $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * create the cache for the bearer token for ONE API
     * lifetime = 4 hours
     *
     * @return FilesystemAdapter
     */
    private function getCacheForBearerToken()
    {
        $directory = $this->kernel->getProjectDir() . self::BUSINESS_CACHE;
        return new FilesystemAdapter(self::NAMESPACE, self::LIFE_TIME, $directory);
    }

    /**
     * get bearer token from the cache
     * @return ItemInterface
     * @throws InvalidArgumentException
     */
    public function getCachedBearerToken()
    {
        $cache = $this->getCacheForBearerToken();
        return $cache->getItem(self::KEY_NAME);
    }

    /**
     * add bearer token to the cache
     * @param ItemInterface $item
     */
    public function addBearerTokenToCache(ItemInterface $item)
    {
        $cache = $this->getCacheForBearerToken();
        $cache->save($item);
    }

    /**
     * clear the cache
     * @return void
     */
    public function clearCachedBearerToken()
    {
        $cache = $this->getCacheForBearerToken();
        $cache->clear();
    }
}